### Danniel Rolfe  
Senior Software Engineer

P. 1 (971) 971-713-1358   
Ex. [talkto@dannielrolfe.com](mailto:talkto@dannielrolfe.com?subject=Hello Danniel)  
W. [dannielrolfe.com](http://www.dannielrolfe.com)

## The focus of my work is building web applications that meet business requirements while maintaining modern development best practices.

### Skills 
* Angular 7
* React 
* Javascript / Typescript / Java
* NodeJs
* Spring Boot 
* TDD
* Docker
* Agile Development

### Experience
**Software Engineer III | The Standard | Portland, Or | May 2018 - Current**
* Contributed to the development of several Angular 5 applications 
* Provided code review to colleagues to improve code quality  
* Collaborated with product owner and project manager to define scope of work via Agile methodologies 
* Mentored developers 

**UI Developer - Consultant  | Nike | Portland, Or | February 2018 - May 2018**
* Contributed to the development of a React and Springboot application 
* Provided code review to colleagues to improve code quality  
* Collaborated with product owner and project manager to define scope of work via Agile methodologies
* Mentored developers

**Full Stack Developer - Consultant  | The Standard | Portland, Or | Aug 2017 - Current**
* Contributed to the development of several Angular 5 applications 
* Provided code review to colleagues to improve code quality  
* Collaborated with product owner and project manager to define scope of work via Agile methodologies 

**Front-end Developer - Consultant | 17Hats | Hillsboro (Remote), Or | Mar 2017 - June 2017**
* Responsible for the development of various features in React 
* Collaborated with a team of developers, project managers, product owners, and UX/UI designers in an Agile environment
* Collaborate with the customer support team to help streamline bug fixing 

**Full Stack Developer - Consultant | Manifes | Hillsboro, Or | Mar 2017 - June 2017**
* Responsible for the development of several applications in Node.js, Angular.js, and Laravel
* Collaborated with a team of developers in an Agile environment
* Contributed to a data watermarking application, a custom e-commerce website, analaytics applicaiton, and an E-Learning platform used by over 50,000 students to date

**UX Developer - Consultant  | Oregon Catholic Press | Portland, OR | May 2016 - Aug 2016**
* Managed a team front-end developers using Agile methodologies
* Responsible for the user experience design and frontend development of the company’s e-commerce website valued at $2 million
* Provided development and user experience direction to various departments

**Consultant | Rolfe Consulting | Hillsboro, OR | Sep 2012 - Feb 2017**
* Worked with medium to large companies by building custom applications that met complex business requirements
* Consulting projects included working with clients such as Hewlett-Packard, Sitka Center for Art and Ecology, Risq Energy Drink, NWCPE, and Tinitron
* Employed a fulltime project coordinator and various contractors

**Systems Information Manager | Tinitron Inc. | Hillsboro, OR | Jun 2014 - Feb 2015**

**Marketing Coordinator | Zeroday, LLC | Wilsonville, OR | Jul 2012 - Dec 2013**

### Education
Bachelor of Business Administration  
Portland State University 2008-2012

### Presentations
* Reactive Progamming with Angular - Angular Meetup - August 16, 2018





